<?php

namespace App\Http\Controllers;

use App\Entities\Buyer;
use App\Entities\Order;
use App\Entities\OrderItem;
use App\Entities\Product;
use App\Http\Resources\OrderResource;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::paginate(10);

        return OrderResource::collection($orders);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $validator = $this->prepareValidator();

        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }

        $data = $validator->validated();

        $buyer = Buyer::find($data['buyerId']);
        $orderItems = $this->createOrderItems($data['orderItems']);
        $order = new Order;

        $buyer->orders()->save($order);
        $order->items()->saveMany($orderItems);

        return new OrderResource($order->refresh());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);

        if (!$order) {
            return new Response([
                'result' => 'fail',
                'message' => 'order not found'
            ]);
        }

        return new OrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $validator = $this->prepareValidator();

        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }

        $data = $validator->validated();

        $order = Order::find($id);

        if (!$order) {
            return new Response([
                'result' => 'fail',
                'message' => 'order not found'
            ]);
        }

        $order->items()->delete();

        $orderItems = $this->createOrderItems($data['orderItems']);

        $order->items()->saveMany($orderItems);

        return new OrderResource($order->refresh());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = Order::destroy($id);

        return new Response([
            'result' => $result ? 'success' : 'fail'
        ]);
    }

    protected function createOrderItems(array $itemsData)
    {
        return collect($itemsData)->map(function ($itemData) {
            $product = Product::find($itemData['productId']);

            if ($itemData['productDiscount'] > 0) {
                $sum = round(ceil($product->price * (1 - $itemData['productDiscount'] / 100)) * $itemData['productQty']);
            } else {
                $sum = round($product->price * $itemData['productQty']);
            }

            $attributes = [
                'quantity' => $itemData['productQty'],
                'discount' => $itemData['productDiscount'],
                'price' => $product->price,
                'sum' => $sum,
                'product_id' => $itemData['productId']
            ];

            return new OrderItem($attributes);
        });
    }

    protected function prepareValidator()
    {
        return validator(request()->all(), [
            'buyerId' => ['sometimes', 'required', 'integer', 'min:1', 'exists:buyers,id'],
            'orderItems' => ['present', 'array'],
            'orderItems.*.productId' => ['sometimes', 'required', 'integer', 'min:1', 'exists:products,id'],
            'orderItems.*.productQty' => ['sometimes', 'required', 'integer', 'min:1'],
            'orderItems.*.productDiscount' => ['sometimes', 'required', 'integer', 'between:0,100']
        ]);
    }
}
