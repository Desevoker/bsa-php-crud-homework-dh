<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'orderId' => $this->id,
            'orderDate' => Carbon::parse($this->created_at)->format('d-m-Y'),
            'orderSum' => optional($this->items)->reduce(function ($orderSum, $item) {
                return round($orderSum + ($item->sum / 100), 2);
            }),
            'orderItems' => optional($this->items, function ($items) {
                return OrderItemResource::collection($items);
            }),
            'buyer' => new BuyerResource($this->buyer)
        ];
    }
}
