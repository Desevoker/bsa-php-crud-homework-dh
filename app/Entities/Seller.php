<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $fillable = ['name'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
