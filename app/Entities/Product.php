<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $attributes = [
        'available' => false
    ];

    protected $fillable = ['name', 'price', 'available', 'seller_id'];

    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }
}
