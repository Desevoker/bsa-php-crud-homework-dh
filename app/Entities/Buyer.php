<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    protected $fillable = ['name', 'surname', 'country', 'city', 'addressLine', 'phone'];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
