<?php

use Illuminate\Database\Seeder;
use App\Entities\Product;
use App\Entities\Seller;

class SellerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Seller::class, 10)->create()->each(function ($seller) {
            $seller->products()->saveMany(
                factory(Product::class, 10)->make()
            );
        });
    }
}
