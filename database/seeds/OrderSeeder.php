<?php

use Illuminate\Database\Seeder;
use App\Entities\Buyer;
use App\Entities\Order;
use App\Entities\OrderItem;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Buyer::class, 5)->create()->each(function ($buyer) {
            $buyer->orders()->saveMany(
                factory(Order::class, 10)->create(['buyer_id' => $buyer->id])->each(function ($order) {
                    $order->items()->saveMany(
                        factory(OrderItem::class, rand(1, 5))->make()
                    );
                })
            );
        });
    }
}
