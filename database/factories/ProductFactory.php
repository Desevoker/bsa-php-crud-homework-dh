<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entities\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->text(10),
        'price' => $faker->numberBetween(100, 10000),
        'available' => $faker->boolean()
    ];
});
