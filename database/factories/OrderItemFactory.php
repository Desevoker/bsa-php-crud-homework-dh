<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entities\OrderItem;
use App\Entities\Product;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'quantity' => $faker->numberBetween(1, 20),
        'discount' => $faker->numberBetween(0, 50),
        'product_id' => function () {
            do {
                $product = Product::find(rand(1, 100));
            } while (!$product);

            return $product->id;
        },
        'price' => function (array $orderItem) {
            return Product::find($orderItem['product_id'])->price;
        },
        'sum' => function (array $orderItem) {
            if ($orderItem['discount'] > 0) {
                return round(ceil($orderItem['price'] * (1 - $orderItem['discount'] / 100)) * $orderItem['quantity']);
            }

            return round($orderItem['price'] * $orderItem['quantity']);
        }
    ];
});
