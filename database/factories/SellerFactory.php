<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entities\Seller;
use Faker\Generator as Faker;

$factory->define(Seller::class, function (Faker $faker) {
    return [
        'name' => $faker->name
    ];
});
